#!/usr/bin/env python

'''
THE GUESS NUMBER
By mocatz 2016
based on the trainning : http://knightlab.northwestern.edu/2014/06/05/five-mini-programming-projects-for-the-python-beginner/
'''
from random import randint

def player_answer(number, guessNumber):
	if number > guessNumber:
		print ("Your number is to high")
	elif number < guessNumber:
		print("Your number is to low")
	else :
		print("Congratulation ! You found the guess number.")

def change_value():
	print("Set min value :")
	minValue = int(input())
	print("Set max value :")
	maxValue = int(input())
	while minValue > maxValue or minValue == maxValue :
		print("the min value can't be inferior or equal to the max value")
		maxValue = int(input())
	print ("Choose a number between " + str(minValue) + " and " + str(maxValue) +" included")
	return randint(minValue, maxValue)

guess_number = randint(0, 10)
player_number = 0

print("Choose a number between 0 and 10 included")

while int(player_number) != guess_number:
	print("-------------------------")
	player_number = input()
	if str(player_number) == "option":
		player_number = 0
		guess_number = change_value()
	else :
		player_answer(int(player_number), guess_number)
		if int(player_number) != guess_number :
			print("Retry!")

